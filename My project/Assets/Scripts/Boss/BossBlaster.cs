using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBlaster : MonoBehaviour
{
    public GameObject BossLaser;
    public Transform BossFirePoint;

    public float startDelay = 2.0f;
    public float spawnInterval = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("EnemyShoot", startDelay, spawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void EnemyShoot()
    {
        Instantiate(BossLaser, BossFirePoint.transform.position, BossLaser.transform.rotation);
    }
}
