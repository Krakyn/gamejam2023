using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class BossDeath : MonoBehaviour
{
    private BossLifeManager bossLifeManager; //Reference life manager
    public int lifeToGive; //Amount the score increases
    public ParticleSystem explosionParticle; //Store the particle system

    // Start is called before the first frame update
    void Start()
    {
        bossLifeManager = GameObject.Find("BossLifeManager").GetComponent<BossLifeManager>(); //Get LifeManager Component
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("Laser")) 
       {
            Destroy(other.gameObject); //Destroy laser
            Explosion();
            bossLifeManager.IncreaseLife(lifeToGive); //Increase life
       }

      
    }

    void Explosion()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);
    }
    
}
