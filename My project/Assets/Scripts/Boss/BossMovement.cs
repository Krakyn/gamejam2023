using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovement : MonoBehaviour
{
    public float speed;
    private bool isSwitch;
    public float distanceThresh;
    public Vector3 targetPositionA;
    public Vector3 targetPositionB;


    // Start is called before the first frame update
    void Start()
    {
        isSwitch = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSwitch == false)
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
        }
        if (isSwitch == true)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }

        if (Vector3.Distance(transform.position, targetPositionA) < distanceThresh)
        {
            isSwitch = true;
        }
        if (Vector3.Distance(transform.position, targetPositionB) < distanceThresh)
        {
            isSwitch = false;
        }
    }
}
