using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class BossEndGame : MonoBehaviour
{
    public TextMeshProUGUI lifeCount; 
    public bool isGameOver;
    public LifeManager lifeManager;
    public bool isGameWon;
    public BossLifeManager bossLifeManager;
    public TextMeshProUGUI bossLives;


    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
        isGameWon = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(lifeManager.life == 0)
        {
            isGameOver = true;
        }
        if(isGameOver == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
        if(bossLifeManager.life == 0)
        {
            isGameWon = true;
        }
        if(isGameWon == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

}
