using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class BossSwitch : MonoBehaviour
{
    public TextMeshProUGUI score; 
    public bool isBossTime;
    public ScoreManager scoreManager;


    // Start is called before the first frame update
    void Start()
    {
        isBossTime = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(scoreManager.score > 4999)
        {
            isBossTime = true;
        }
        if(isBossTime == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
        }
    }

}