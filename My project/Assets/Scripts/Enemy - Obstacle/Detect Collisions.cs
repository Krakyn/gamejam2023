using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollisions : MonoBehaviour
{
    private ScoreManager scoreManager; //Reference score manager
    public int scoreToGive; //Amount the score increases
    public ParticleSystem explosionParticle; //Store the particle system
    private LifeManager lifeManager;
    public int lifeToGive;

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>(); //Get ScoreManager Component
        lifeManager = GameObject.Find("LifeManager").GetComponent<LifeManager>();
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("Player")) 
       {
            Destroy(gameObject); //Destroy Obstacle
            //Destroy(other.gameObject); //Destroy laser
            Explosion();
            scoreManager.IncreaseScore(scoreToGive); //Increase Score
       }
       if(other.gameObject.CompareTag("Player") && gameObject.CompareTag("Enemy"))
       {
            Destroy(gameObject);
            Explosion();
            lifeManager.IncreaseLife(lifeToGive);
       }

      
    }

    void Explosion()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);
    }
    
}
