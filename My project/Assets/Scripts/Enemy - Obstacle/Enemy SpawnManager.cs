using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    public GameObject[] enemyPrefabs; //This array will hold more than one enemy
    public float spawnRangeX = 17.0f;
    public float spawnPosZ = 15.0f;
    public float startDelay = 10.0f;
    public float spawnInterval = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        spawnInterval = Random.Range(1,6);
        InvokeRepeating("SpawnRandomEnemy", startDelay, spawnInterval);
    }

    void SpawnRandomEnemy()
    {
        //Generate a position to spawn at on the x-axis
        Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
        //Pick a random enemy from the array to spawn
        int enemyIndex = Random.Range(0,enemyPrefabs.Length);
        //Spawn the enemy indexed from the array
        Instantiate(enemyPrefabs[enemyIndex], spawnPos, enemyPrefabs[enemyIndex].transform.rotation);
    }
}