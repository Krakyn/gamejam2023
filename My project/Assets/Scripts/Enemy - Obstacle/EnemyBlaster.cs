using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBlaster : MonoBehaviour
{
    public GameObject EnemyLaser;
    public Transform Enemy_FirePoint;

    public float startDelay = 6.0f;
    public float spawnInterval = 4.0f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("EnemyShoot", startDelay, spawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void EnemyShoot()
    {
        Instantiate(EnemyLaser, Enemy_FirePoint.transform.position, EnemyLaser.transform.rotation);
    }
}
