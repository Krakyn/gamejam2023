using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour
{
    private ScoreManager scoreManager; //Reference score manager
    public int scoreToGive; //Amount the score increases
    public ParticleSystem explosionParticle; //Store the particle system

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>(); //Get ScoreManager Component
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("Laser")) 
       {
            Destroy(gameObject); //Destroy UFO
            Destroy(other.gameObject); //Destroy laser
            Explosion();
            scoreManager.IncreaseScore(scoreToGive); //Increase Score
       }

      
    }

    void Explosion()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);
    }
    
}
