using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawnManager : MonoBehaviour
{
    public GameObject[] obstaclePrefabs; //This array will hold more than one obstacle
    public float spawnRangeX = 17.0f;
    public float spawnPosZ = 15.0f;
    private float startDelay = 2.0f;
    public float spawnInterval = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnRandomObstacle", startDelay, spawnInterval);
    }

    void SpawnRandomObstacle()
    {
        //Generate a position to spawn at on the x-axis
        Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
        //Pick a random enemy/UFO from the array to spawn
        int obstacleIndex = Random.Range(0,obstaclePrefabs.Length);
        //Spawn the enemy indexed from the array
        Instantiate(obstaclePrefabs[obstacleIndex], spawnPos, obstaclePrefabs[obstacleIndex].transform.rotation);
    }
}
