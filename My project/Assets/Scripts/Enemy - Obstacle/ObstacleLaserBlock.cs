using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleLaserBlock : MonoBehaviour
{
    public ParticleSystem explosionParticle; //Store the particle system

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("Laser") || other.gameObject.CompareTag("EnemyLaser")) 
       {
            Destroy(other.gameObject); //Destroy Obstacle
            //Destroy(other.gameObject); //Destroy laser
            Explosion();
       }

      
    }

    void Explosion()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);
    }
    
}
