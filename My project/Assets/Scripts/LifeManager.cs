using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LifeManager : MonoBehaviour
{
    public int life; //store life value
    public TextMeshProUGUI lifeText; //Reference visual text ui element to change

    //Rewards player
    public void IncreaseLife(int amount)
    {
        life += amount; //add too life
        UpdateLifeText(); //Update life text
    }

    //Penalize player
    public void DecreaseLife(int amount)
    {
        life -= amount; //subtract amount from the life count
        UpdateLifeText(); //Update life text
    }

    public void UpdateLifeText()
    {
        lifeText.text = "Life: "+ life;
    }
}