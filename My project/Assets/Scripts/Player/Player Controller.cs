using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float hInput; //store horizontal input
    public float vInput; //store vertical input
    public float speed;  // player movement speed
    public float xRange = 17.0f;  //Limit left and right. Won't leave screen
    public float zRange = 8.50f; //Limit up and down
    public GameObject laser; //Reference to the prefab game object to shoot
    public Transform firePoint; //Point of origin for laser bolt

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       //Set horizontal input to recieve values from keyboard keymap "horizontal"
       hInput = Input.GetAxis("Horizontal"); 

       //Move the player left and right
       transform.Translate(Vector3.right * hInput * speed * Time.deltaTime);

       //Set vertical input
       vInput = Input.GetAxis("Vertical");

       //Move player up and down
       transform.Translate(Vector3.forward * vInput * speed * Time.deltaTime);

       //Keep player in bounds
        //Right Side
       if(transform.position.x > xRange)
       {
        transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
       }
        //Left Side
        if(transform.position.x < -xRange)
       {
        transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
       }


        //Top Side
        if(transform.position.z > zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y,zRange);
        }
        //Bottom Side
        if(transform.position.z < -zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y,-zRange);
        }



        //Creates Laser game object at the firepoint
       if(Input.GetKeyDown(KeyCode.Space))
       {
            Instantiate(laser, firePoint.transform.position, laser.transform.rotation);
       }
    }
}
