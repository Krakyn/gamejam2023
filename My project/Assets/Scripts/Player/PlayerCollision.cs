using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    private LifeManager lifeManager; //Reference life manager
    public int lifeToGive; //Amount the score increases
    public ParticleSystem explosionParticle; //Store the particle system

    // Start is called before the first frame update
    void Start()
    {
        lifeManager = GameObject.Find("LifeManager").GetComponent<LifeManager>(); //Get LifeManager Component
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("EnemyLaser")) 
       {
            Destroy(other.gameObject); //Destroy laser
            Explosion();
            lifeManager.IncreaseLife(lifeToGive); //Increase life
       }

      
    }

    void Explosion()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);
    }
    
}
