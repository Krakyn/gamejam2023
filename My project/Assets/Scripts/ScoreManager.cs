using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public int score; //store score value
    public TextMeshProUGUI scoreText; //Reference visual text ui element to change

    //Rewards player
    public void IncreaseScore(int amount)
    {
        score += amount; //add to score
        UpdateScoreText(); //Update score text
    }

    //Penalize player
    public void DecreaseScore(int amount)
    {
        score -= amount; //subtract amount from the score
        UpdateScoreText(); //Update score text
    }

    public void UpdateScoreText()
    {
        scoreText.text = "Score: "+ score;
    }
}